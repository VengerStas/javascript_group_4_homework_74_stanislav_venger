const express = require('express');
const fileDb = require('../fileDb');

const router = express.Router();

router.post('/', (req, res) => {
    fileDb.addItems(req.body.message);
    res.send('OK');
});

module.exports = router;
