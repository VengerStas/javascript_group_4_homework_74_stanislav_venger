const express = require('express');
const fs = require('fs');
const fileDb = require('../fileDb');

const filePath = './messages';

const router = express.Router();

let messagesArray = [];

router.get('/', (req, res) => {
   fs.readdir(filePath, (err, messages) => {
       messages.forEach(message => {
           const getFileMessage = fs.readFileSync(filePath + '/' + message);
           const parsMessage = JSON.parse(getFileMessage);
           messagesArray.push(parsMessage);
       });
   });
    res.send(fileDb.getItems(messagesArray).slice(-5));
});

module.exports = router;
