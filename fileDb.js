const fs = require('fs');
const path = require('path');

module.exports = {
    getItems(messages) {
        return messages;
    },

    addItems(item) {
        const date = new Date();
        const fileName = date.toISOString().replace(/:/g, "");
        const message = {
            message: item,
            date: date
        };
        fs.writeFileSync(path.join('messages', `${fileName}.txt`), JSON.stringify(message, null, 2));
    },
};



