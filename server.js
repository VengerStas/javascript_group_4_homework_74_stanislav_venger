const express = require('express');
const messages = require('./app/messages');
const getMessage = require('./app/getMesasge');

const port = 8000;


const app = express();
app.use(express.json());
app.use('/messages', messages);
app.use('/messages', getMessage);


app.listen(port, () => {
    console.log(port);
});

